package connexion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class ConnexionOSTest {
	
	private ConnexionOS conn;
	private String uri = "http://192.168.149.140:9080/wsi/FNCEWS40MTOM/";
	private String username = "p8admin";
	private String password = "filenet";
	private String domainName = "P8DEV";
	private String osName = "HeiaRERE";

	@Before
	public void loadProp(){
		conn = new ConnexionOS();
		conn.loadProperties();
	}
	
	@Test
	public final void testLoadProperties() {
		assertEquals(username, conn.getUsername());
		assertEquals(password,conn.getPassword());
		assertEquals(uri,conn.getUri());
		assertEquals(domainName,conn.getDomainName());
		assertEquals(osName, conn.getOsName());
	}

	@Test
	public final void testConnectToDomain() {
		conn.connectToDomain();
		assertNotNull(conn.getConnection());
		assertNotNull(conn.getSubject());
		assertNotNull(conn.getUserContext());
		assertNotNull(conn.getDomain());
	}

	@Test
	public final void testConnectToOS() {
		conn.connectToDomain();
		conn.connectToOS();
		assertEquals("P8DEV", conn.getDomainName());
		assertEquals("HeiaRERE", conn.getOsName());
		assertNotNull(conn.getOs());
	}

}
