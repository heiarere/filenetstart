package connexion;

import javax.security.auth.Subject;

import org.apache.log4j.Logger;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;

import tools.PropertiesUtils;

public class ConnexionOS {

	private PropertiesUtils properties;
	private Connection connection;
	private Subject subject;
	private UserContext userContext;
	private Domain domain;
	private ObjectStore os;
	private String uri;
	private String username;
	private String password;
	private String domainName;
	private String osName;
	private final String JAAS = "FileNetP8Engine";
	private static Logger logger = Logger.getLogger(ConnexionOS.class);

	public void loadProperties() {
		properties = new PropertiesUtils();
		properties.loadFile("config.properties");
		setUri(properties.getValueProperty("uri"));
		setUsername(properties.getValueProperty("userName"));
		setPassword(properties.getValueProperty("password"));
		setDomainName(properties.getValueProperty("domainName"));
		setOsName(properties.getValueProperty("osName"));
		logger.info("URI : " + uri);
		logger.info("Username : " + username);
		logger.info("Password : " + password);
		logger.info("Domain name : " + domainName);
		logger.info("Object Store name : " + osName);
	}

	public void connectToDomain() {
		logger.info("Connection to the domain");
		try {
			setConnection(Factory.Connection.getConnection(uri));
			setSubject(UserContext.createSubject(connection, username, password, JAAS));
			setUserContext(UserContext.get());
			userContext.pushSubject(subject);
			setDomain(Factory.Domain.fetchInstance(connection, domainName, null));
		} catch (Exception e) {
			logger.error("Error during the connection to the domain",e);
		}
		
	}

	public void connectToOS() {
		try{
		setOs(Factory.ObjectStore.fetchInstance(domain, osName, null));
		} catch(Exception e){
			logger.error("Error during the connection to the object store : " + getOsName(), e);
		}
	}

	// ********************************************* GETTERS AND SETTERS *******************************************************//

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection
	 *            the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the subject
	 */
	public Subject getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/**
	 * @return the userContext
	 */
	public UserContext getUserContext() {
		return userContext;
	}

	/**
	 * @param userContext
	 *            the userContext to set
	 */
	public void setUserContext(UserContext userContext) {
		this.userContext = userContext;
	}

	/**
	 * @return the domain
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * @return the os
	 */
	public ObjectStore getOs() {
		return os;
	}

	/**
	 * @param os
	 *            the os to set
	 */
	public void setOs(ObjectStore os) {
		this.os = os;
	}

	/**
	 * @param uri
	 *            the uri to set
	 */
	private void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	private void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	private void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param domainName
	 *            the domainName to set
	 */
	private void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @param osName
	 *            the osName to set
	 */
	private void setOsName(String osName) {
		this.osName = osName;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @return the osName
	 */
	public String getOsName() {
		return osName;
	}

	/**
	 * @return the jAAS
	 */
	public String getJAAS() {
		return JAAS;
	}

}
