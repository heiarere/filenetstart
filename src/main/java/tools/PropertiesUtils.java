package tools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesUtils {

	private Properties properties;
	private static Logger logger = Logger.getLogger(PropertiesUtils.class);
	private InputStream input;

	public PropertiesUtils() {
		setProperties(new Properties());
	}

	public void loadFile(String filename) {
		try {
			logger.info("Load file properties " + filename);
			setInput(PropertiesUtils.class.getClassLoader().getResourceAsStream(filename));
			properties.load(getInput());
		} catch (FileNotFoundException e) {
			logger.error("File not found " + filename, e);
		} catch (IOException e) {
			logger.error("Error during the loading of the input file " + filename, e);
		}
	}

	public void displayKeys() {
		Enumeration<Object> enumKey = properties.keys();
		while (enumKey.hasMoreElements()) {
			String key = (String) enumKey.nextElement();
			logger.info("Key : " + key);
		}
	}

	public String getValueProperty(String nameProperty) {
		String value = null;
		try {
			value = properties.getProperty(nameProperty);
		} catch (Exception e) {
			logger.error("Error for retrieving the property : " + nameProperty, e);
		}
		return value;
	}

	public void setValueProperty(String value, String property) {
		logger.info("Initialization of the property : " + property + " with the value : " + value);
		properties.setProperty(property, value);
	}

	// **************************************** GETTERS AND SETTERS ************************************************ //

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public InputStream getInput() {
		return input;
	}

	public void setInput(InputStream input) {
		this.input = input;
	}

}
